#!venv/bin/python3
from pathlib import Path
import enoslib as en
from tansiv.undercloud import g5k, g5k_kvm
from tansiv.api import start_ts, start_ts_kvm, generate_deployment, generate_deployment_kvm, generate_platform, ts_kvm_vsg_start


HERE = Path(__file__).parent.absolute()
QEMU_IMAGE = HERE / "packer/packer-debian-11.6.0-x86_64-qemu/debian-11.6.0-x86_64.qcow2"
DOCKER_IMAGE = "registry.gitlab.inria.fr/tansiv/tansiv/tansiv-main:latest"
BASE_DIR = HERE / "ts"
NB_WORKER = 30

logging = en.init_logging()
cluster_size = NB_WORKER + 1 # + Master node
nb_cores = [1 for _ in range(NB_WORKER + 1)]

xp_descriptor =f"results_deathstarbench"
result_dir = BASE_DIR / xp_descriptor
result_dir.mkdir(parents=True, exist_ok=True)
# First we generate the input files in the result_dir
latency = "2ms"
bandwidth = "1GBps"
generate_platform(latency, bandwidth, result_dir / "platform.xml")
# generate_deployment(cluster_size, result_dir / "deployment.xml")
generate_deployment_kvm(cluster_size, nb_cores, result_dir / "deployment.xml")
# The default g5k parameter to use when launched from the outside
g5k_dict = dict(cluster="paravance", walltime="02:00:00", number=1, queue="default")
# _roles, _ = g5k(**g5k_dict)
_roles, _ = g5k_kvm(**g5k_dict)

# vms, networks = start_ts_kvm(
vms, networks = start_ts_kvm(
    _roles,
    str(result_dir / "platform.xml"),
    str(result_dir / "deployment.xml"),
    qemu_image=QEMU_IMAGE,
    docker_image=DOCKER_IMAGE,
    simgrid_args="--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912"
)

ts_kvm_vsg_start(_roles, NB_WORKER + 1)


# Assign roles
# 1st machine = Master, others = workers
master = vms[0:1]
workers = vms[1:]


# Start Docker swarm on master
master_address = master[0].extra["tansiv_address"]
print(f"master address is {master_address}")
results = en.run_command(f"docker swarm init --advertise-addr {master_address}", roles=master )
for r in results:
    print(r.stdout)
results = en.run_command(f"docker swarm join-token worker -q", roles=master)
for r in results:
    swarm_token=r.stdout
    print(r.stdout)

# Join the swarm on workers
results = en.run_command(f"docker swarm join --token {swarm_token} {master_address}:2377", roles=workers)
for r in results:
    print(r.stdout)

# Start DeathStarBench
results = en.run_command(f"docker node ls", roles=master)
for r in results:
    print(r.stdout)
results = en.run_command(f"cd /home/tansiv/app/DeathStarBench/socialNetwork && docker stack deploy --compose-file=docker-compose-swarm.yml socialNetwork", roles=master)
for r in results:
    print(r.stdout)