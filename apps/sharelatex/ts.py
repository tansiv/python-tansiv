#!venv/bin/python3

#OAR -l nodes=1,walltime=02:00:00
#OAR -t besteffort
#OAR -t idempotent
#OAR -p cluster='paravance'


from pathlib import Path

import enoslib as en

from tansiv.undercloud import insideout
from tansiv.api import start_ts, generate_deployment, generate_platform
from tansiv.sharelatex import sharelatex_setup, SERVICES

HERE = Path(__file__).parent.absolute()
QEMU_IMAGE = HERE / "packer/packer-alpine-3.15.2-docker-x86_64-qemu/alpine-3.15.2-docker-x86_64.qcow2"
DOCKER_IMAGE = "registry.gitlab.inria.fr/tansiv/tansiv/tansiv-master:latest"
BASE_DIR = HERE / "ts"

USERS = 10

logging = en.init_logging()

cluster_size = len(SERVICES) + 1

xp_descriptor = f"results_sharelatex"
result_dir = BASE_DIR / xp_descriptor
result_dir.mkdir(parents=True, exist_ok=True)

# First we generate the input files in the result_dir
latency = "20ms"
bandwidth = "1GBps"
generate_platform(latency, bandwidth, result_dir / "platform.xml")
generate_deployment(cluster_size, result_dir / "deployment.xml")


# The default g5k parameter to use when launched from the outside
g5k_dict = dict(cluster="paravance", walltime="02:00:00", number=1, queue="default")
_roles, _ = insideout(g5k_dict)

vms, networks = start_ts(
    _roles,
    str(result_dir / "platform.xml"),
    str(result_dir / "deployment.xml"),
    qemu_image=QEMU_IMAGE,
    docker_image=DOCKER_IMAGE,
    simgrid_args="--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912"
)

roles = sharelatex_setup(vms[0:-1])
roles["utils"] = [vms[-1]]

# create some users
deltas = []
for i in range(USERS):
    r = en.run(
        f"docker exec --workdir /var/www/sharelatex/web/modules/server-ce-scripts/scripts -i web node create-user-with-pass.js --email=joe{i}@inria.fr --pass=TestTest42",
        roles["web"],
    )
    deltas.append(r[0].payload["delta"])
print(deltas)
