#!/usr/bin/env bash

set -eux

# remove automatic startup

case $SERVICE in
web)
  echo "WEB"
  rm /etc/my_init.d/00_regen_sharelatex_secrets.sh
  ;;
*)
  echo "DEFAULT"
  rm -rf /etc/runit/runsvdir/default/sshd
  rm -rf /etc/runit/runsvdir/default/nginx
  rm -rf /etc/runit/runsvdir/default/cron
  # remove any startup script
  # we don't want to replay the migrations nor regenerate some secrets
  cd /etc/my_init.d
  ls /etc/my_init.d | grep -v 00_make_sharelatex_data_dirs.sh | xargs -n1 rm
  ;;
esac

# remove all X-sharelatex where X isn't $SERVICE
cd /etc/runit/runsvdir/default
ls -d *sharelatex| grep -v $SERVICE* | xargs rm -rf
