"""Sharelatex deployment in a dispersed way

1 service per docker_container per VM.
VM base image contains all the required services and must be generated with the
associated packer project.
"""
import enoslib as en

_ = en.init_logging()

FORCE_DEPLOY = False

# claim the resources
SERVICES = [
    "chat",
    "clsi",
    "contacts",
    "docstore",
    "documentupdater",
    "filestore",
    "notifications",
    "realtime",
    "spelling",
    "trackchanges",
    "web",
    "mongo",
    "redis",
]

USERS = 10


conf = (
    en.VMonG5kConf.from_settings(
        job_name="tansiv",
        image="/home/msimonin/public/alpine-3.15.2-docker-x86_64.qcow2",
        # image="/home/msimonin/public/generic-alpine315-libvirt",
        force_deploy=FORCE_DEPLOY,
        walltime="03:00:00",
    )
    # aio
    .add_machine(
        roles=["xp"], cluster="paravance", number=len(SERVICES) + 1, flavour="tiny"
    )
    .finalize()
)


provider = en.VMonG5k(conf)


_roles, _networks = provider.init()


assert len(SERVICES) + 1 == len(_roles["xp"])

roles = en.Roles()
service_ips = {}
for idx, service in enumerate(SERVICES):
    h = _roles["xp"][idx]
    h.set_extra(service=service)
    service_ips.update({service: h.address})
    roles[service] = [h]

# all
roles["xp"] = _roles["xp"]
# only overleaf
roles["overleaf"] = _roles["xp"][0:-1]
roles["utils"] = [_roles["xp"][-1]]
print(service_ips)

roles = en.sync_info(roles=roles, networks=_networks)

with en.actions(roles=roles["overleaf"]) as a:
    for (service, ip) in service_ips.items():
        a.lineinfile(path="/etc/hosts", line=f"{ip} {service}")
    a.copy(src="settings.env", dest="/tmp/settings.env")
    a.copy(src="settings.js", dest="/tmp/settings.js")
    a.docker_container(
        name="{{ service }}",
        image="registry.gitlab.inria.fr/tansiv/python-tansiv/{{ service }}-tansiv-overleaf:latest",
        state="started",
        env_file="/tmp/settings.env",
        volumes=["/tmp/settings.js:/etc/sharelatex/settings.js"],
        network_mode="host",
    )


# add some network constraints
n = en.Netem().add_constraints("delay 20ms", hosts=roles["xp"], networks=_networks["enos_network"])
n.deploy()

# create some users
deltas = []
for i in range(USERS):
    r = en.run(
        f"docker exec --workdir /var/www/sharelatex/web/modules/server-ce-scripts/scripts -i web node create-user-with-pass.js --email=joe{i}{i}@inria.fr --pass=TestTest42",
        roles["web"],
    )
    deltas.append(r[0].payload["delta"])
print(deltas)
