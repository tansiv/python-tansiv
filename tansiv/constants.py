"""
Some common default.

Intent: gives some consistent settings between different scripts
"""
from pathlib import Path
import os


ROOT_DIR = Path(__file__).parent.parent.resolve()

# fallback on our setting
TS_QEMU_ARGS = os.environ.get(
    "QEMU_ARGS", "-icount shift=0,sleep=off,align=off -rtc clock=vm"
)
NTS_QEMU_ARGS = os.environ.get("QEMU_ARGS", "")

# To configure : smp settings + monitor
TS_KVM_QEMU_ARGS = os.environ.get(
    "QEMU_ARGS", "-accel kvm -cpu max,invtsc=on"
)

DEFAULT_QEMU_MEM = os.environ.get("QEMU_MEM", "1g")

BOOT_CMD = os.environ.get("BOOT_CMD", "tanboot")

AUTOCONFIG_NET = False

DEFAULT_DOCKER_IMAGE = "registry.gitlab.inria.fr/tansiv/tansiv/tansiv-main"

DEFAULT_KERNEL_ARTIFACTS = "https://gitlab.inria.fr/api/v4/projects/41022/jobs/artifacts/main/download?job=kernel"

DEFAULT_CLUSTER = "ecotype"
