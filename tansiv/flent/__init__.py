import gzip
import json
from pathlib import Path
import shutil
import time
from typing import List, Union

import enoslib as en


def flent(
    hosts: List[en.Host],
    working_dir: Path = Path("results"),
    duration: int = 60,
    benchs=["tcp_upload"],
):
    # split the hosts
    masters = hosts[0::2]
    workers = hosts[1::2]
    for i, worker in enumerate(workers):
        worker.extra.update(flent_server=masters[i].extra["tansiv_alias"])

    with en.actions(roles=masters) as p:
        p.shell("pkill -9 netserver || true")
        p.shell("tmux new-session -s netserver -d 'netserver -D'")

    for bench in benchs:

        remote_dir = f"{bench}_{str(time.time_ns())}"
        start = time.time()
        with en.actions(roles=workers) as p:
            p.file(state="directory", path=f"{remote_dir}")
            # recording the "real" time
            p.shell(
                " ".join(
                    [
                        f"flent {bench}",
                        f"-p totals -l {duration}",
                        "-H {{ flent_server }}",
                        # Makes fping fail
                        # "--local-bind {{ tansiv_address }}",
                        "--socket-stats",
                        "-f csv",
                    ]
                ),
                chdir=remote_dir,
            )
        end = time.time()
        # some results
        working_dir.mkdir(parents=True, exist_ok=True)
        (working_dir / f"timing_{remote_dir}").write_text(str(end - start))
        with en.actions(roles=workers) as p:
            p.shell(f"tar -czf {remote_dir}.tar.gz {remote_dir}")
            p.fetch(src=f"{remote_dir}.tar.gz", dest=f"{working_dir}")


def unpack(env_dir: Union[Path, str]) -> Path:
    """Unpack all the tar.gz found in env_dir.

    Put the resulting file in unpacked sub dir.
    """
    env_dir = Path(env_dir)
    unpacked_dir = env_dir / "unpacked"
    if unpacked_dir.exists():
        shutil.rmtree(unpacked_dir)
    unpacked_dir.mkdir(parents=True, exist_ok=True)
    import tarfile

    for targz in env_dir.rglob("*.tar.gz"):
        file = tarfile.open(targz)
        file.extractall(str(unpacked_dir))
        file.close()
    # now look inside the unpacked and ... unpack again
    for gz in unpacked_dir.rglob("*.gz"):
        file = (unpacked_dir / gz.name).with_suffix("")
        file.write_bytes(gzip.decompress(gz.read_bytes()))
    return unpacked_dir


def to_dfs(env_dir: str):
    """Translate flent reports into DataFrames."""
    import pandas as pd

    unpacked_dir = unpack(env_dir)
    dfs = []
    for result in unpacked_dir.glob("*.flent"):
        _result = json.loads(result.read_text())
        data = _result["results"]
        data["t"] = _result["x_values"]
        data["orig"] = str(result)
        data["key"] = _result["metadata"]["NAME"]
        df = pd.DataFrame(data)
        dfs.append(df)
    return dfs
