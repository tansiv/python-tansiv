"""
Currently different api function to experiment with Tansiv are implemented

Deploy
------

- reserve a single node on G5K
- boot some VMs using :
    - tansiv
        - fork/exec some VMs and put simgrid as a bridge between them
    - notansiv
        - fork/exec some VMs and put a regular bridge in-between
- Fill some datastructure to record the current configuration

Emulate
-------

- Emulate some network latencies between the VMs
    + internally (qdisc inside the VMs are modified)
    + externally (WiP -- qdisc modification are set outside the VM / on the host bridge)

Flent
-----

- Run some flent generic benchmark and gather results

Fping
-----

- Run an fping and gather the results

"""
from ipaddress import IPv4Interface, ip_interface
import json
from tansiv.constants import DEFAULT_QEMU_MEM, DEFAULT_KERNEL_ARTIFACTS
from typing import List, Optional, Tuple
from pathlib import Path
import time


import enoslib as en
from enoslib.objects import DefaultNetwork, Host, Networks
from enoslib.infra.provider import Provider


class TantapNetwork(DefaultNetwork):
    pass


class MantapNetwork(DefaultNetwork):
    pass


# Our two networks
## one network to communicate using through a tantap or a tap
TANTAP_NETWORK = TantapNetwork("192.168.1.0/24")
# Management network (using a tap)
MANTAP_NETWORK = MantapNetwork("10.0.0.0/24")

# indexed them (EnOSlib style)
NETWORKS = Networks(tantap=[TANTAP_NETWORK], mantap=[MANTAP_NETWORK])


class TansivHost(en.Host):
    """
    A TansivHost is an EnOSlib Host but requires an (optional) extra ssh jump to
    access it

    local_machine --ssh1--> g5k machine --ssh2--> tansiv host
    ssh1 also requires a jump through the access machine but that should be
    already set in your .ssh/config
    ssh2 is what we handle here.

    ssh2 is optional because the undercloud might be directly accessible. For
    instance when using LocalHost as the undercloud.
    """

    def __init__(
        self,
        mantap_address: str,
        mantap_alias: str,
        mantap_mac_address: str,
        tantap_address: str,
        tantap_alias: str,
        tantap_mac_address: str,
        g5k_machine: en.Host,
    ):
        super().__init__(mantap_address, alias=mantap_alias, user="root")
        self.extra.update(
            tansiv_alias=tantap_alias,
            tansiv_address=tantap_address,
            tantap_mac_address=tantap_mac_address,
            mantap_mac_address=mantap_mac_address
        )

        if not isinstance(g5k_machine, en.LocalHost):
            # inject a mean to connect to the g5k_machine first
            # + some ad'hoc variables
            self.extra.update(
                gateway=g5k_machine.address,
                gateway_user="root",
            )


def build_tansiv_roles_from_deployment(
    deployment: Path, tansiv_node: en.Host
) -> List[Host]:
    """Build enoslib roles based on a simgrid deployment file.

    Args:
        deployment: Path to the deployment file
        tansiv_node: the Host representing the node where tansiv is launched

    Returns
        The roles representing the virtual machines launched by tansiv
        according to the deployment file.
    """
    # build the inventory based on the deployment file in use
    import xml.etree.ElementTree as ET

    tree = ET.parse(str(deployment))
    root = tree.getroot()
    mantap_ifaces = sorted(
        [
            IPv4Interface(e.attrib["value"])
            for e in root.findall("./actor/argument[last()]")
        ]
    )
    tantap_ifaces = sorted(
        [
            IPv4Interface(e.attrib["value"])
            for e in root.findall("./actor/argument[last()-1]")
        ]
    )

    tantap_mac_start = "02:ca:fe:f0:0d:"
    mantap_mac_start = "54:52:fe:f0:0d:"

    tansiv_hosts = [
        TansivHost(
            mantap_address = str(mantap_iface.ip),
            mantap_alias = f"mantap{mantap_iface.ip.packed[-1]}",
            mantap_mac_address = mantap_mac_start + f"{hex(n+10).lstrip('0x').rjust(2, '0')}",
            tantap_address = str(tantap_iface.ip),
            tantap_alias = f"tantap{mantap_iface.ip.packed[-1]}",
            tantap_mac_address = tantap_mac_start + f"{hex(n+10).lstrip('0x').rjust(2, '0')}",
            g5k_machine = tansiv_node,
        )
        for mantap_iface, tantap_iface, n in zip(mantap_ifaces, tantap_ifaces, range(len(mantap_ifaces)))
    ]
    print(tansiv_hosts)
    return tansiv_hosts


def build_tansiv_roles(number: int, tansiv_node: en.Host) -> List[Host]:
    # WARNING: there's an implicit here
    mantap_iface_start = ip_interface("10.0.0.10/24")
    tantap_iface_start = ip_interface("192.168.1.10/24")
    tantap_mac_start = "02:ca:fe:f0:0d:"
    mantap_mac_start = "54:52:fe:f0:0d:"
    hosts = []
    for n in range(number):
        mantap_iface = mantap_iface_start + n
        tantap_iface = tantap_iface_start + n
        mantap_mac = mantap_mac_start + f"{hex(n+10).lstrip('0x').rjust(2, '0')}"
        tantap_mac = tantap_mac_start + f"{hex(n+10).lstrip('0x').rjust(2, '0')}"
        hosts.append(
            TansivHost(
                mantap_address = str(mantap_iface.ip),
                mantap_alias = f"mantap{mantap_iface.ip.packed[-1]}",
                mantap_mac_address = mantap_mac,
                tantap_address = str(tantap_iface.ip),
                tantap_alias = f"tantap{mantap_iface.ip.packed[-1]}",
                tantap_mac_address = tantap_mac,
                g5k_machine = tansiv_node,
            )
        )
    print(hosts)
    return hosts


def _start_internal(
    cmd: str,
    env: dict[str, str],
    p: en.actions,
    qemu_image: str,
    docker_image: str,
    container_name: Optional[str] = "tansiv"
):
    """Internal method / factorization purpose."""
    # copy my ssh key
    pub_key = Path.home() / ".ssh" / "id_rsa.pub"
    if not pub_key.exists() or not pub_key.is_file():
        raise Exception(f"No public key found in {pub_key}")

    # copy the pub_key
    p.copy(src=str(pub_key), dest="/tmp/id_rsa.pub")
    p.file(path="/tmp/tansiv_image", state="directory")
    p.synchronize(
        src=str(qemu_image),
        # don't make this part of /tmp/tansiv since it's wiped every time
        dest="/tmp/tansiv_image/image.qcow2",
        task_name="copying base image",
    )

    p.docker_container(
        state="started",
        network_mode="host",
        name=container_name,
        image=docker_image,
        volumes=[
            "/tmp/id_rsa.pub:/root/.ssh/id_rsa.pub",
            "/tmp/tansiv:/srv/tansiv",
            "/tmp/tansiv_image:/srv/tansiv_image",
        ],
        capabilities=["NET_ADMIN"],
        devices=["/dev/net/tun"],
        command=cmd,
        env=env,
        restart=True,
        # For KVM
        privileged=True,
        pid_mode="host",
    )

    # by default packets that needs to be forwarded by the bridge are sent to iptables
    # iptables will most likely drop them.
    # we can disabled this behaviour by bypassing iptables
    # https://wiki.libvirt.org/page/Net.bridge.bridge-nf-call_and_sysctl.conf
    # We have two bridges currently
    # - the tantap bridge: only used for traffic not supported by the vsg implementation (e.g arp request, dhcp)
    # - the mantap bridge: used for management tasks, traffic follow a
    #   regular flow through the bridge so might be dropped by iptables (e.g ping from m10 to m11)
    p.sysctl(
        name="net.bridge.bridge-nf-call-iptables",
        value="0",
        state="present",
        sysctl_set="yes",
    )
    p.sysctl(
        name="net.bridge.bridge-nf-call-arptables",
        value="0",
        state="present",
        sysctl_set="yes",
    )


def start_ts(
    roles: en.Roles,
    platform: str,
    deployment: str,
    qemu_image: str,
    docker_image: str,
    simgrid_args: Optional[str] = "",
    env: Optional[dict[str, str]] = {},
    docker_registry_opts: Optional[dict] = None,
):
    """Starts ts

    There will as many ts as nodes in roles.

    Args:
        roles: roles on which a ts will be deployed
        platform: the simgrid platform
        deployment; the simgrid deployment
        qemu_image: the base image to use for the vms
        mode: tap|tantap. If mode==tap the platform and deployment is ignored
        docker_image: the docker image to use (needs tansiv and python-tansiv in it)
        qemu_args: extra args to pass
        env: extra environment variables to set in the Docker container
    """

    command = f"tansiv /srv/tansiv/platform.xml /srv/tansiv/deployment.xml --log=vm_interface.threshold:debug --log=vm_coordinator.threshold:debug {simgrid_args}"
    # install docker
    docker = en.Docker(
        agent=roles["tansiv"],
        bind_var_docker="/tmp/docker",
        registry_opts=docker_registry_opts,
    )
    docker.deploy()
    with en.actions(roles=roles) as p:
        p.file(path="/tmp/tansiv", state="absent")
        p.file(path="/tmp/tansiv", state="directory")

        p.synchronize(
            src=platform,
            dest="/tmp/tansiv/platform.xml",
            task_name="copying platform file",
        )
        p.synchronize(
            src=deployment,
            dest="/tmp/tansiv/deployment.xml",
            task_name="copying deployment file",
        )
        _start_internal(command, env, p, qemu_image, docker_image)
    # This is specific to tantap
    tansiv_hosts = build_tansiv_roles_from_deployment(
        Path(deployment), roles["tansiv"][0]
    )

    en.wait_for(roles=tansiv_hosts)
    return tansiv_hosts, NETWORKS


def deploy_kernel(hosts: List[Host]):
    # Install modules
    results = en.run_command(
        "cd /root && tar -xf modules.tar && cp -r /root/lib/modules/*/ /lib/modules",
        task_name="Untar kernel modules artifacts",
        roles=hosts
        )
    for r in results:
        print(r.stdout)
    # Install kernel
    results = en.run_command("depmod -a", task_name="Install kernel (depmod)", roles=hosts)
    for r in results:
        print(r.stdout)
    results = en.run_command("update-initramfs -k all -c", task_name="Install kernel (update-initramfs)", roles=hosts)
    for r in results:
        print(r.stdout)
    results = en.run_command("""sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="/&iommu=on /' /etc/default/grub""", roles=hosts)
    results = en.run_command("update-grub", task_name="Install kernel (grub)", roles=hosts)
    for r in results:
        print(r.stdout)
   

def deploy_ts_kvm(
    roles: en.Roles,
    docker_image: str,
    docker_registry_opts: Optional[dict] = None,
    artifacts_url: Optional[str] = DEFAULT_KERNEL_ARTIFACTS,
):
    """Deploys ts_kvm

    This function installs the custom kernel, the kernel module required by
    tanqemukvm, and installs Docker

    Args:
        roles: roles on which a ts will be deployed
        platform: the simgrid platform
        deployment; the simgrid deployment
        qemu_image: the base image to use for the vms
        mode: tap|tantap. If mode==tap the platform and deployment is ignored
        docker_image: the docker image to use (needs tansiv and python-tansiv in it)
        qemu_args: extra args to pass
    """
    
    results = en.run_command(
        "apt-get update && apt-get install -y kmod git unzip curl socat",
        task_name="Install packages for kernel deployment",
        roles=roles
        )
    for r in results:
        print(r.stdout)
    
    results = en.run_command(
        f"cd /root && curl --location {artifacts_url} --output artifacts.zip",
        task_name="Recover kernel artifacts",
        roles=roles
        )
    for r in results:
        print(r.stdout)
    
    results = en.run_command(
        "cd /root && unzip artifacts.zip && cp ci-kernel/vmlinuz* /boot && cp tansiv/src/kvm/Kernelspace/*.ko .",
        task_name="Unzip and move artifacts",
        roles=roles
        )
    for r in results:
        print(r.stdout)

    deploy_kernel(roles)
    with en.actions(roles=roles) as p:
        p.reboot()
    en.wait_for(roles=roles)
    
    results = en.run_command(
        "insmod /root/*.ko",
        task_name="Install tansiv kernel module",
        roles=roles
        )
    for r in results:
        print(r.stdout)
    
    # install docker
    docker = en.Docker(
        agent=roles["tansiv"],
        bind_var_docker="/tmp/docker",
        registry_opts=docker_registry_opts,
    )
    docker.deploy()

    # pull docker image
    results = en.run_command(
        f"docker pull {docker_image}",
        task_name="Pull tansiv docker image",
        roles=roles
        )
    for r in results:
        print(r.stdout)

    # create rsa key
    results = en.run_command(
        "ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa",
        task_name="Generate rsa ssh key",
        roles=roles
        )
    for r in results:
        print(r.stdout)
    

def start_ts_kvm(
    roles: en.Roles,
    platform: str,
    deployment: str,
    qemu_image: str,
    docker_image: str,
    container_name: Optional[str] = "tansiv",
    simgrid_args: Optional[str] = "",
    tansiv_args: Optional[str] = "",
    env: Optional[dict[str, str]] = {},
):
    """Starts ts_kvm

    There will as many ts as nodes in roles.

    Args:
        roles: roles on which a ts will be deployed
        platform: the simgrid platform
        deployment; the simgrid deployment
        qemu_image: the base image to use for the vms
        mode: tap|tantap. If mode==tap the platform and deployment is ignored
        docker_image: the docker image to use (needs tansiv and python-tansiv in it)
        qemu_args: extra args to pass
        env: extra environment variables to set in the Docker container
    """
    command = f"tansiv /srv/tansiv/platform.xml /srv/tansiv/deployment.xml {tansiv_args} --log=vm_interface.threshold:debug --log=vm_coordinator.threshold:debug  {simgrid_args}"
    with en.actions(roles=roles) as p:
        p.file(path="/tmp/tansiv", state="absent")
        p.file(path="/tmp/tansiv", state="directory")

        p.synchronize(
            src=platform,
            dest="/tmp/tansiv/platform.xml",
            task_name="copying platform file",
        )
        p.synchronize(
            src=deployment,
            dest="/tmp/tansiv/deployment.xml",
            task_name="copying deployment file",
        )
        _start_internal(command, env, p, qemu_image, docker_image, container_name=container_name)

    # This is specific to tantap
    tansiv_hosts = build_tansiv_roles_from_deployment(
        Path(deployment), roles["tansiv"][0]
    )

    en.wait_for(roles=tansiv_hosts)

    return tansiv_hosts, NETWORKS


def ts_kvm_vsg_start(roles: en.Roles, nb_vms: int):
    for i in range(nb_vms):
        en.run_command(
            f"""echo "vsg-start" | socat - unix-connect:/tmp/tansiv/qemu-monitor-{i}""",
             task_name=f"vsg start on VM {i}",
             roles=roles)


def start_nts(
    roles: en.Roles,
    number: int,
    qemu_image: str,
    docker_image: str,
    qemu_args: Optional[str] = None,
    qemu_mem: str = DEFAULT_QEMU_MEM,
    env: Optional[dict[str, str]] = {},
    docker_registry_opts: Optional[dict] = None,
):
    """Start a no ts.

    There will as many ts as nodes in roles and as many VMs as number in each TS.

    Args:
        roles: roles on which a ts will be deployed
        number: the number of VMs to start
        qemu_image: the base image to use for the VMS
        docker_image: the docker image to use (needs tansiv and python-tansiv in it)
        qemu_args: extra args to pass
        env: extra environment variables to set in the Docker container
    """
    print(roles)
    docker = en.Docker(
        agent=roles["tansiv"],
        bind_var_docker="/tmp/docker",
        registry_opts=docker_registry_opts,
    )
    docker.deploy()
    with en.actions(roles=roles) as p:

        p.file(path="/tmp/tansiv", state="absent")
        p.file(path="/tmp/tansiv", state="directory")
        command = [
            "notansiv.py",
            "--qemu_cmd tanqemu-system-x86_64",
            f"--qemu_mem {qemu_mem}",
            f"--qemu_image /srv/tansiv_image/image.qcow2",
            "--autoconfig_net",
            # shared with the external world
            "--base_working_dir /srv/tansiv",
            f"--number {number}",
        ]
        if qemu_args:
            # covers None or empty str
            command.append(f"--qemu_args '{qemu_args}'")
        _start_internal(" ".join(command), env, p, qemu_image, docker_image)
    tansiv_roles = build_tansiv_roles(number, roles["tansiv"][0])
    en.wait_for(roles=tansiv_roles)
    return tansiv_roles, NETWORKS


def background_shell(provider: Provider, session_name: str, command_line: str):
    provider.shell(
        f"(tmux ls | grep {session_name} ) ||tmux new-session -s {session_name} -d '{command_line}'"
    )


def background_shell_kill(provider: Provider, session_name: str):
    provider.shell(f"tmux kill-session -t {session_name}")


def fping(hosts: List[Host]):
    # dummy validation
    # -- runs fping and get point to point latency for every pair of nodes
    # -- assuming that mXX is the name of the machine on the management interface
    # -- assuming that tXX is the name of the machien on the tansiv interface
    # Start tansiv on the containers
    hostnames = [h.alias for h in hosts] + [h.extra["tansiv_alias"] for h in hosts]
    print(hostnames)
    results = en.run_command(
        f'fping -q -b 1000 -C 30 -s -e {" ".join(hostnames)}',
        roles=hosts,
    )

    # displayng the output (the result datastructure is a bit painful to parse...ask enoslib maintainer)
    for r in results:
        print(f"################## <{r.host}/{r.task}]> #################")
        # fping stats are displayed on stderr
        print(r.stderr)
        print(f"################## </{r.host}/{r.task}> #################")


def destroy(provider: Provider, hosts: List[Host], force: True, container_name: Optional[str] = "tansiv"):
    if force:
        provider = provider
        provider.destroy()
    else:
        # be kind / soft removal
        with en.actions(roles=hosts) as p:
            p.docker_container(
                name=container_name, state="absent", task_name="Removing tansiv container", timeout=180
            )


def vm_emulate(options: str, hosts: List[Host], tansiv_networks: en.Networks):
    # sync_info is still getting true Roles as input
    roles = en.sync_info(en.Roles(all=hosts), tansiv_networks)
    hosts = roles["all"]
    netem = en.Netem().add_constraints(
        options,
        hosts=hosts,
        symetric=True,
        networks=tansiv_networks["tantap"],
    )
    netem.deploy()


def host_emulate(options: str, hosts: List[Host]):
    # FIXME
    cout = en.NetemOutConstraint("tantap-br", options)
    cin = en.NetemInConstraint("tantap-br", options)
    source = en.NetemInOutSource(hosts[0], constraints=set([cin, cout]))
    en.netem([source])


def dump(hosts: List[Host], tansiv_hosts: List[Host], working_dir: Path):
    host = dict(
        docker_ps=en.run_command("docker ps", roles=hosts).to_dict(),
        docker_inspect=en.run_command("docker inspect tansiv", roles=hosts).to_dict(),
        ps_qemu=en.run_command("ps aux | grep qemu", roles=hosts).to_dict(),
        qdisc=en.run_command("tc qdisc", roles=hosts).to_dict(),
    )
    vms = dict(qdisc=en.run_command("tc qdisc", roles=tansiv_hosts).to_dict())
    dump = dict(host=host, vms=vms)
    (working_dir / "dump").write_text(json.dumps(dump))


def generate_deployment(
    size: int, out: Path, template: str = "deployment.xml.j2", **kwargs
):
    """Generate a simgrid deployment.

    Args:
        size: number of VMs to generate
        out: path where the file will be generated
        template: input template
        kwargs: any key/value that needs to be replaced in the template (e.g
        qemu_mem)
    """
    from jinja2 import Environment, FileSystemLoader, select_autoescape

    env = Environment(
        loader=FileSystemLoader(Path(__file__).parent / "templates"),
        autoescape=select_autoescape(),
    )
    template = env.get_template(template)
    out.parent.mkdir(parents=True, exist_ok=True)
    out.write_text(template.render(descriptors=range(size), **kwargs))


def generate_deployment_kvm(
    size: int, nb_cores: list[int], out: Path, template: str = "deployment-qemukvm.xml.j2", **kwargs
):
    """Generate a simgrid deployment (For tanqemukvm).

    Args:
        size: number of VMs to generate
        nb_cores: List of length size, where the i-th element is the number of
        cores for the i-th VM
        out: path where the file will be generated
        template: input template
        kwargs: any key/value that needs to be replaced in the template (e.g
        qemu_mem)
    """
    from jinja2 import Environment, FileSystemLoader, select_autoescape

    env = Environment(
        loader=FileSystemLoader(Path(__file__).parent / "templates"),
        autoescape=select_autoescape(),
    )
    # Auto-generate MAC addresses
    mac = ["02:ca:fe:f0:0d:{:02x}".format(120+i) for i in range(size)]
    template = env.get_template(template)
    out.parent.mkdir(parents=True, exist_ok=True)
    out.write_text(template.render(descriptors=zip(range(size), nb_cores, mac), **kwargs))


def generate_platform(latency: str, bandwidth: str, out: Path, template: str = "star.xml.j2", **kwargs):
    """
    Args:
      latency: latency to set on the links (eg 0.1E-1s)
      bandwidth: bandwidth to set on the links (eg 10Gbps)
    """
    from jinja2 import Environment, FileSystemLoader, select_autoescape

    env = Environment(
        loader=FileSystemLoader(Path(__file__).parent / "templates"),
        autoescape=select_autoescape(),
    )
    template = env.get_template(template)
    out.parent.mkdir(parents=True, exist_ok=True)
    out.write_text(template.render(latency=latency, bandwidth=bandwidth, **kwargs))
