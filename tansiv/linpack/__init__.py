from re import A
import enoslib as en
from pathlib import Path

from typing import List, Union


LINPACK_INPUT = """Shared-memory version of Intel(R) Distribution for LINPACK* Benchmark. *Other names and brands may be claimed as the property of others.
Sample data file lininput_xeon64.
1                     # number of tests
1000
1000
1 # times to run a test
1 # alignment values (in KBytes)

"""

CMD = "/opt/linpack/l_mklb_p_2018.3.011/benchmarks_2018/linux/mkl/benchmarks/linpack/xlinpack_xeon64"


def linpack(
    hosts: List[en.Host], working_dir: Path, linpack_input: str = LINPACK_INPUT
) -> float:
    """Runs linpack on nodes.

    linpack must be installed on the node (in /opt/linpack)
    ~wget http://registrationcenter-download.intel.com/akdlm/irc_nas/9752/l_mklb_p_2018.3.011.tgz && tar xf l_mklb_p_2018.3.011.tgz~


    Args
        hosts:
            EnOSlib's hosts
        linpack_input:
            Content of the benchmark's input


    Returns:
        average GFlops
    """
    with en.actions(roles=hosts) as a:
        a.copy(dest="/tmp/linpack_input", content=linpack_input)
        a.shell(f"{CMD} /tmp/linpack_input | tee /tmp/linpack.out")
        a.fetch(src="/tmp/linpack.out", dest=str(working_dir / "linpack_results"))


def parse_result(linpack_result_dir: Union[str, Path]) -> float:
    import re

    avgs = []
    for f in Path(linpack_result_dir).rglob("*.out"):
        lines = f.open("r").readlines()
        for i in range(len(lines)):
            l = lines[i]
            if l.startswith("Size   LDA    Align.  Average  Maximal"):
                print(lines[i + 1])
                m = re.match(r"\d+ +\d+ + \d+ +(\d+.\d+) +.*", lines[i + 1])
                avgs.append(float(m.group(1)))
    return sum(avgs) / len(avgs)
