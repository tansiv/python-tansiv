import argparse
import logging
import traceback
from tansiv import linpack


from tansiv.api import NETWORKS
from tansiv.constants import *
from tansiv.tasks import (
    deploy,
    start_nts,
    start_ts,
    start_ts_kvm,
    flent,
    linpack,
    fping,
    vm_emulate,
    host_emulate,
    dump,
    destroy,
    generate_deployment,
)

import enoslib as en

en.init_logging()


def register(module_name, subparsers):
    from importlib import import_module

    module = import_module(f"tansiv.cli.{module_name}")
    return getattr(module, "register")(subparsers)


def register_ts(subparsers):
    parser_ts = subparsers.add_parser("ts", help="Deploy a ts")
    parser_ts.add_argument(
        "platform",
        help="The simgrid plaform file",
    )
    parser_ts.add_argument(
        "deployment",
        help="The simgrid deployment file",
    )
    parser_ts.add_argument(
        "--simgrid_args",
        type=str,
        default="",
        help="extra simgrid args to pass ex: '--cfg=network/model:CM02'",
    )

    parser_ts.set_defaults(mode_func=start_ts)
    parser_ts.set_defaults(func=cli_deploy)
    return parser_ts

def register_ts_kvm(subparsers):
    parser_ts_kvm = subparsers.add_parser("ts_kvm", help="Deploy a ts with KVM")
    parser_ts_kvm.add_argument(
        "platform",
        help="The simgrid plaform file",
    )
    parser_ts_kvm.add_argument(
        "deployment",
        help="The simgrid deployment file",
    )
    parser_ts_kvm.add_argument(
        "--simgrid_args",
        type=str,
        default="",
        help="extra simgrid args to pass ex: '--cfg=network/model:CM02'",
    )

    parser_ts_kvm.set_defaults(mode_func=start_ts_kvm)
    parser_ts_kvm.set_defaults(func=cli_deploy)
    return parser_ts_kvm

def register_nts(subparsers):
    parser_nts = subparsers.add_parser("nts", help="Deploy a nts")
    parser_nts.add_argument("--qemu_args", help="Some qemu_args", default=NTS_QEMU_ARGS)
    parser_nts.add_argument("number", help="Number of VMs to deploy", type=int)

    parser_nts.set_defaults(mode_func=start_nts)
    parser_nts.set_defaults(func=cli_deploy)
    return parser_nts


def deploy_arguments(parser):
    """Common arguments."""
    parser.add_argument(
        "--docker_image",
        help="Tansiv docker image to use",
        default=DEFAULT_DOCKER_IMAGE,
    )
    parser.add_argument(
        "--qemu_image", help="Base image to use (qcow2)", required=True
    )


def cli_deploy(args, env=None):
    """Compose the functions to get a ts/nts deployed on a infrastructure."""
    undercloud_func = args.undercloud_func
    # enostask ahead
    deploy(undercloud_func, args, env=env)
    args.mode_func(args, env=env)


def main():
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description="Tansiv experimentation engine")
    parser.add_argument("--env", help="env directory to use")

    # deploy --env <required> [--cluster ...] tansiv
    # deploy --env <required> [--cluster ... ] notansiv
    # ------------------------------------------------------------------- DEPLOY
    subparsers = parser.add_subparsers(help="deploy")
    parser_deploy = subparsers.add_parser(
        "deploy", help="Deploy tansiv and the associated VMs"
    )
    # common arges
    deploy_arguments(parser_deploy)

    undercloud_parsers = []
    undercloud_subparsers = parser_deploy.add_subparsers(help="system deployment")
    undercloud_parsers.append(register("g5k", undercloud_subparsers))
    undercloud_parsers.append(register("localhost", undercloud_subparsers))

    for undercloud_parser in undercloud_parsers:
        # inject a default enoslib.task to deploy the undercloud
        # this allows for a shortcut to be taken on the cli (tan deploy g5k)
        from functools import partial

        undercloud_parser.set_defaults(
            func=partial(cli_deploy, undercloud_parser.get_default("undercloud_func"))
        )
        # handle mode of operation tansiv(ts)/no tansiv(nts)
        subparsers_deploy = undercloud_parser.add_subparsers(
            help="mode of operation ts|nts"
        )
        register_ts(subparsers_deploy)
        register_ts_kvm(subparsers_deploy)
        register_nts(subparsers_deploy)

    # --------------------------------------------------------------------------

    # -------------------------------------------------------------------- TS
    ts_parser = register_ts(undercloud_subparsers)
    ts_parser.set_defaults(func=start_ts)
    # --------------------------------------------------------------------------

    # -------------------------------------------------------------------- TS_KVM
    ts_kvm_parser = register_ts_kvm(undercloud_subparsers)
    ts_kvm_parser.set_defaults(func=start_ts_kvm)
    # --------------------------------------------------------------------------

    # -------------------------------------------------------------------- NTS
    nts_parser = register_nts(undercloud_subparsers)
    nts_parser.set_defaults(func=start_nts)
    # --------------------------------------------------------------------------

    # -------------------------------------------------------------------- FPING
    parser_fping = subparsers.add_parser("fping", help="Run a fping in full mesh")
    parser_fping.set_defaults(func=fping)
    # --------------------------------------------------------------------------

    # -------------------------------------------------------------------- FLENT
    parser_flent = subparsers.add_parser("flent", help="Run flent")
    parser_flent.set_defaults(func=flent)
    parser_flent.add_argument(
        "--duration", help="duration (seconds) of the bench", default=60
    )
    # --------------------------------------------------------------------------

    # -------------------------------------------------------------------- LINPACK
    parser_linpack = subparsers.add_parser("linpack", help="Run linpack")
    parser_linpack.set_defaults(func=linpack)
    # --------------------------------------------------------------------------

    # ------------------------------------------------------------------ VM_EMULATE
    parser_emulate = subparsers.add_parser("vm_emulate", help="emulate")
    parser_emulate.set_defaults(func=vm_emulate)
    parser_emulate.add_argument(
        "options", help="The options to pass to our (Simple)Netem (e.g 'delay 10ms')"
    )

    # ------------------------------------------------------------------ HOST_EMULATE
    parser_emulate = subparsers.add_parser("host_emulate", help="emulate")
    parser_emulate.set_defaults(func=host_emulate)
    parser_emulate.add_argument(
        "options", help="The options to pass to our (Simple)Netem (e.g 'delay 10ms')"
    )

    # ------------------------------------------------------------------ DUMP
    parser_dump = subparsers.add_parser("dump", help="Dump some infos")
    parser_dump.set_defaults(func=dump)

    # ------------------------------------------------------------------ DESTROY
    parser_destroy = subparsers.add_parser("destroy", help="Destroy the deployment")
    parser_destroy.add_argument(
        "--force",
        action="store_true",
        help="Remove the remote running tansiv container. Forcing will free the provided resources",
    )
    parser_destroy.set_defaults(func=destroy)
    # --------------------------------------------------------------------------

    # ---------------------------------------------------------------------- GEN
    parser_gen = subparsers.add_parser(
        "gen", help="Generate the deployment file (wip)"
    )
    parser_gen.add_argument(
        "size",
        help="Size of the deployment",
    )
    parser_gen.add_argument(
        "out",
        help="Output file",
    )
    parser_gen.add_argument(
      "--template",
      help="deployment template name to use",
      default="deployment.xml.j2"
    )
    parser_gen.set_defaults(func=generate_deployment)
    # --------------------------------------------------------------------------
    args = parser.parse_args()

    try:
        if getattr(args, "func", None):
            args.func(args, env=args.env)
        else:
            parser.print_help()
    except Exception as e:
        parser.print_help()
        print(e)
        traceback.print_exc()


if __name__ == "__main__":
    main()
