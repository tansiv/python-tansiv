"""

Examples:

.. code-block:: python

    # deploy
    python g5k.py --env tantap_icount deploy \
        inputs/nova_cluster.xml inputs/deployment_2.xml \
        --cluster ecotype  \
        --docker_image registry.gitlab.inria.fr/msimonin/2018-vsg/tansiv:d8fa9110e5d8fcd936a0497fe6dee2b2a09337a2  \
        --walltime=05:00:00 \
        --mode=tantap

    # benchmark it
    python g5k.py flent
"""

from pathlib import Path

import enoslib as en

import tansiv.api as api


def generate_deployment(args, env=None) -> str:
    """Generate a deployment file with size vms.

    Many things remain hardcoded (e.g physical host to map the processes).
    """
    size = int(args.size)
    out = Path(args.out)
    template = args.template
    api.generate_deployment(size, out, template=template)


def generate_deployment_kvm(args, env=None) -> str:
    """Generate a deployment file with size vms for tanqemukvm.

    Many things remain hardcoded (e.g physical host to map the processes).
    """
    size = int(args.size)
    nb_cores = list(args.nb_cores)
    out = Path(args.out)
    template = args.template
    api.generate_deployment_kvm(size, nb_cores, out, template=template)


@en.enostask()
def start_ts(args, env=None):
    """cli shortcut support."""
    tansiv_roles, tansiv_networks = api.start_ts(
        env["roles"],
        args.platform,
        args.deployment,
        args.qemu_image,
        args.docker_image,
        simgrid_args=args.simgrid_args,
    )
    env["tansiv_roles"] = tansiv_roles
    env["tansiv_networks"] = tansiv_networks


@en.enostask()
def start_ts_kvm(args, env=None):
    """cli shortcut support."""
    tansiv_roles, tansiv_networks = api.start_ts_kvm(
        env["roles"],
        args.platform,
        args.deployment,
        args.qemu_image,
        args.docker_image,
        simgrid_args=args.simgrid_args,
    )
    env["tansiv_roles"] = tansiv_roles
    env["tansiv_networks"] = tansiv_networks


@en.enostask()
def start_nts(args, env=None):
    """cli shortcut support."""
    tansiv_roles, tansiv_networks = api.start_nts(
        env["roles"],
        args.number,
        args.qemu_image,
        args.docker_image,
        qemu_args=args.qemu_args,
    )
    env["tansiv_roles"] = tansiv_roles
    env["tansiv_networks"] = tansiv_networks


@en.enostask(new=True)
def deploy(undercloud_func, args, env=None):
    roles, provider = undercloud_func(args)
    env["roles"] = roles
    env["provider"] = provider


@en.enostask()
def fping(args, env=None):
    """Validates the deployment.

    Idempotent.
    Only run fping on the remote hosts to get a matrix of latencies.
    """
    tansiv_roles = env["tansiv_roles"]
    api.fping(tansiv_roles)


@en.enostask()
def flent(args, env=None):
    """Runs flent."""
    tansiv_roles = env["tansiv_roles"]
    working_dir = env.env_name
    api.flent(tansiv_roles, working_dir=working_dir, duration=args.duration)


@en.enostask()
def linpack(args, env=None):
    """Runs linpack."""
    tansiv_roles = env["tansiv_roles"]
    working_dir = env.env_name
    from tansiv.linpack import linpack

    linpack(tansiv_roles, working_dir)


@en.enostask()
def destroy(args, env=None):
    force = args.force
    roles = env["roles"]
    provider = env["provider"]
    api.destroy(provider, roles, force)


@en.enostask()
def vm_emulate(args, env=None):
    """Emulate the network condition.

    The emulation is internal to the VMs: the qdisc of the internal interfaces are modified.

    Homogeneous constraints for now.
    Note that we have options to set heterogeneous constraints in EnOSlib as well.
    """
    options = args.options
    tansiv_roles = env["tansiv_roles"]
    tansiv_networks = env["tansiv_networks"]

    api.vm_emulate(options, tansiv_roles, tansiv_networks)


@en.enostask()
def host_emulate(args, env=None):
    """Emulate the network condition.

    The emulation is external to the VMs: the qdisc of the bridge
    is modified on the host machine.
    (WiP)
    """
    options = args.options
    roles = env["roles"]
    api.host_emulate(options, roles)


@en.enostask()
def dump(args, env=None):
    """Dump some environmental informations."""
    # First on the host machine
    roles = env["roles"]
    tansiv_roles = env["tansiv_roles"]
    working_dir = env.env_name
    api.dump(roles, tansiv_roles, working_dir)