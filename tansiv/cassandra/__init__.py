import copy
import time
from typing import List

import enoslib as en


DOCKER_PREFIX = "docker exec cassandra"

# https://github.com/brianfrankcooper/YCSB/tree/ce3eb9ce51c84ee9e236998cdd2cefaeb96798a8/cassandra
YCSB_CQL = """
create keyspace if not exists ycsb WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor': 3 };
use ycsb;
create table if not exists usertable (
         y_id varchar primary key,
         field0 varchar,
         field1 varchar,
         field2 varchar,
         field3 varchar, 
         field4 varchar,
         field5 varchar,
         field6 varchar,
         field7 varchar,
         field8 varchar,
         field9 varchar);
"""


class Cassandra:
    def __init__(self, hosts: List[en.Host], networks: List[en.Network]):
        # avoid mutations...
        self.hosts = copy.deepcopy(hosts)
        self.networks = networks
        # calculate a list of seeds (take the whole list of possible addresses)
        self.seeds = [h.filter_addresses(networks)[0] for h in hosts]

    def deploy(self):
        """
        Prerequisites:
        - ycsb available in /opt
        - en.sync_info must have been run (we could run it internally to this function if needed)
        """
        # inject some host vars
        for h in self.hosts:
            address = str(h.filter_addresses(self.networks)[0].ip.ip)
            h.set_extra(
                listen_address=address,
                rpc_address=address,
                seeds=",".join([str(a.ip.ip) for a in self.seeds]),
            )

        with en.actions(roles=self.hosts) as a:
            a.docker_container(
                name="cassandra",
                image="cassandra:4",
                state="started",
                network_mode="host",
                env=dict(
                    CASSANDRA_LISTEN_ADDRESS="{{ listen_address }}",
                    CASSANDRA_RPC_ADDRESS="{{ rpc_address }}",
                    CASSANDRA_SEEDS="{{ seeds }}",
                    CASSANDRA_CLUSTER_NAME="Tansiv Cluster",
                    CASSANDRA_NUM_TOKENS="16",
                ),
                # /opt is where ycsb is located
                volumes=["/srv:/srv"],
            )
            a.wait_for(host="{{ rpc_address }}", port=9042, state="started")
            # make ycsb available from the cassandra container
            a.shell("cp -r /opt/ycsb* /srv")

            # prepare ycsb
        self.run_cql(YCSB_CQL)

    def run_cql(self, commands):
        """Runs a command using cqlsh within the cassandra container.

        It connects to the first cassandra node and run the cql commands from there.
        """
        # get the address of the head node
        # since w
        address = str(self.hosts[0].filter_addresses(self.networks)[0].ip.ip)
        with en.actions(roles=self.hosts[0]) as a:
            tmpfile = f"/srv/{time.time_ns()}"
            a.copy(content=commands, dest=tmpfile)
            cmd = f"{DOCKER_PREFIX} cqlsh {address} --file {tmpfile}"
            a.shell(cmd)
        return en.run_command(cmd, roles=self.hosts[0])

    def run_cmd(self, command: str):
        """Runs a shell within the cassandra cluster.


        It connects to the first cassandra node and run the shell commands from there.
        """
        return en.run_command(f"{DOCKER_PREFIX} {command}", roles=self.hosts[0])
