python-tansiv
=============

Python project serving as an entry point for a collection of helper functions
to evaluate a Tansiv System (TS).


What's a Tansiv System (TS)
===========================

At the infrastructure level, a TS is composed of some QEMU virtual machines
(VMs). But, instead of communicating through a regular network those VMs
communicate through Simgrid. A TS system is thus an hybrid system where your
real application runs on top of a simulated network.


- a Tansiv System:

    .. code-block:: bash

        +------------+                       +------------+
        |            |                       |            |
        |   my app   |                       |   my app   |
        |   process1 | <==== SIMGRID =====>  |   process2 |
        |            |                       |            |
        |        vm1 |                       |         vm2|
        +------------+                       +------------+

- Sometime for comparison study we'll consider a No Tansiv System (NTS).
  In this case the virtual machines are linked using a regular linux bridge (in
  the future we may want to evaluate NTS with other options offered by QEMU like
  sockets)


    .. code-block:: bash

        +------------+                       +------------+
        |            |                       |            |
        |   my app   |                       |   my app   |
        |   process1 | <== Linux Bridge ==>  |   process2 |
        |            |                       |            |
        |        vm1 |                       |         vm2|
        +------------+                       +------------+

In a docker
===========

.. code-block:: bash

    laptop) docker run  --device /dev/net/tun --cap-add NET_ADMIN -v $(pwd)/packer:/srv/packer -ti tansiv-ops:latest 

NTS
---

.. code-block:: bash

    docker) ./notansiv.py --qemu_cmd qemu-system-x86_64 --qemu_mem 1g --qemu_image packer/packer-debian-10-x86_64-qemu/debian-10-x86_64.qcow2 --autoconfig_net

TS
--


So, What's here ?
=================

A **minimal** command line is provided, get the help with ``tang --help``.

This CLI basically allows for

- deploying a TS on Grid'5000 (``tan deploy --mode tantap ...``)
    + launch some evaluation tasks on top of it (see ``tan --help`` for available options)
- starting a NTS on Grid'5000 (``tan deploy --mode tap ...``)
- other util stuffs


How can I install it ?
======================

For instance on G5K (with automatic reservation of one node)


- One time setup:

    .. code-block:: python

        $frontend$) git clone git+https://gitlab.inria.fr/tansiv/python-tansiv.git
        $frontend$) cd python-tansiv
        $frontend$) virtualenv -p python3 venv
        $frontend$) source venv/bin/activate
        $frontend$)(venv) pip install -U -e .

- Launch a TS:

    .. code-block:: python

        $frontend$)(venv) tan --env myxp deploy --qemu_image /grid5000/virt-images/debian10-x64-min-2021041419.qcow2 g5k ts inputs/star.xml inputs/deployment_2.xml
        # --env myxp             : the directory where some of the experiment data will be saved
        # inputs/star.xml: a platform file (simulated machines and network characteristics)
        # inputs/deployment_2.xml  : the mapping between virtual machines and host in the plaform file
        # image                  : the virtual machine image (will be copied from the local machine to the g5k node)

- Launch a NTS:


    .. code-block:: python

        $frontend$)(venv) tan --env myxp2 deploy g5k nts 2

        # The last argument is the number of VMs to launch


- Benchmark using flent


    .. code-block:: python

        $frontend$)(venv) tan --env myxp flent


Analyse (on g5k)
================


- (One time setup) Register the python-tansiv virtual environment as a usable jupyter lab kernel.

.. code-block:: bash

    $frontend$) source venv/bin/activate
    $frontend$)(venv) ipython kernel install --name tansiv_venv --user

- Start the jupyter notebook in g5k: https://intranet.grid5000.fr/notebooks

- Change the kernel to the ``tansiv_venv``
